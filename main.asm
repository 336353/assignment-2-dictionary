%include "lib.inc"
%include "words.inc"
extern find_word

%define buffer_size 255
%define dq_size 8

section .rodata

BUFFER_OVERFLOW: db "Слово не помещается в буфер", 0xA, 0
NOT_FOUND: db "Слово не найдено", 0xA, 0


section .text

global _start

_start:
  ; сначала прочитаем ключ из stdin

  mov rsi, buffer_size
  dec rsp
  sub rsp, buffer_size; заводим место для буфера в стеке
  mov rdi, rsp; buffer address

  call read_word; читаем строку из stdin в буфер

  inc rsp
  add rsp, buffer_size; возвращаем исходное состояние стека
  ; в rax адрес буфера, если прочитано, иначе 0
  ; в rdx длина прочитанной строки
  cmp rax, 0
  jz .buffer_overflow

  ; теперь ищем слово в словаре

  mov rdi, rax; адрес на начало нуль-терминированной строчки
  mov rsi, next_word; указатель на начало словаря
  push rdx

  call find_word; в rax адрес начала вхождения в словарь (не значения),
                ; если найдено, иначе 0
  pop rdx
  test rax, rax
  jz .not_found

  add rax, dq_size; скипаем dq с адрес на некст эл-та
  add rax, rdx; скипаем значение ключа
  inc rax; скипаем нуль-терминатор после ключа
  mov rdi, rax

  call print_string
  call exit

  .buffer_overflow:
    mov rdi, BUFFER_OVERFLOW
    call print_error
    call exit
  .not_found:
    mov rdi,NOT_FOUND
    call print_error
    call exit
