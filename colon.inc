%define next_word 0
%macro colon 2
  %2:
    dq next_word
    db %1, 0
    %define next_word %2
%endmacro
