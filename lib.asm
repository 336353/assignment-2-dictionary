global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .rodata

%define SYS_WRITE 1
%define SYS_READ 0
%define SYS_EXIT 60
%define STDERR_CODE 2
%define STDOUT_CODE 1
%define STDIN_CODE 0
%define NULL_TERM 0
%define TO_ASCII 48
%define LAST_BYTE_MASK 0xFF
%define NEXT_LINE 0xA
%define DEC 10
%define MINUS_ASCII 45
%define NULL_ASCII 48
%define NINE_ASCII 57
%define SPACE_ASCII 0x20
%define TAB_ASCII 0x9


section .text


; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax,
    mov rax, SYS_EXIT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; указатель (адрес) на строку лежит в rdi
string_length:
    xor rax, rax
    .loop:
      mov rsi, [rdi]; в rsi теперь очередной элемент строки
                    ; косвенная адресация - в rsi содержимое ячейки с адресом rdi
      and rsi, LAST_BYTE_MASK; символом являются последние 8 битов
                   ; остальные же 56 первых бита - мусорные
                   ; но они все равно будут считываться
                   ; с помощью маски мы их очищаем
      test rsi, rsi; проверка - может в rsi лежит нуль-терминатор
      jz .exit
      inc rdi; получаем указатель на следующий символ
      inc rax; в rax количество символов
      jmp .loop
    .exit:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov r10, rdi; указатель на строку в r10
    mov rsi, r10; адрес, с которого начинается строка
    mov rax, SYS_WRITE
    mov rdi, STDOUT_CODE
    mov rdx, 1; вывод одного символа
    .loop:
      mov r11, [rsi]; в r11 кладем очередной символ по адресу rsi
      and r11, LAST_BYTE_MASK
      test r11, r11
      jz .exit

      syscall

      inc rsi
      jmp .loop

    .exit:
      ret

; Принимает указатель на нуль-терминированную строку с сообщением
; об ошибке, выводит её в stderr
print_error:
    xor rax, rax
    mov r10, rdi; указатель на строку в r10
    mov rsi, r10; адрес, с которого начинается строка
    mov rax, SYS_WRITE
    mov rdi, STDERR_CODE
    mov rdx, 1; вывод одного символа
    .loop:
      mov r11, [rsi]; в r11 кладем очередной символ по адресу rsi
      and r11, LAST_BYTE_MASK
      test r11, r11
      jz .exit

      syscall

      inc rsi
      jmp .loop

    .exit:
      ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    mov rax, SYS_WRITE
    push rdi
    mov rsi, rsp; получаем адрес на значение rdi, лежащее в стеке
    mov rdi, STDOUT_CODE
    mov rdx, 1; выводим один символ
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, NEXT_LINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:

    xor rax, rax
    mov rax, rdi; число для вывода
    test rax, rax
    jz .print_null
    ; в стеке мы будем формировать посимвольно строку
    dec rsp
    mov byte [rsp], NULL_TERM
    mov r10, DEC; основание десятичной СС
    mov r11, 1; количество символов, нужно для возврата стека в исходн. полож.
    .next_symb:
      ; div Делитель; Делимое в RDX:RAX, частное в RAX, остаток в RDX
      xor rdx, rdx
      div r10; отделяем очередную цифру
      add rdx, TO_ASCII; код десятичной СС для перевода в ASCII
      test rdx, rdx
      jz .print_null
      dec rsp
      mov [rsp], dl
      inc r11
      test rax, rax; если частное = 0, то получена последняя цифра
      jnz .next_symb
    .print_unumber:
      mov rdi, rsp; указатель на первую цифру
      push r11
      call print_string
      pop r11
      add rsp, r11; возврат стека в исходное положение
      jmp .exit
    .print_null:
      mov rdi, NULL_ASCII
      call print_char
      jmp .exit
    .exit:
      ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    test rdi, rdi
    js .negative; переход, если SF = 1 - флаг знака
    jmp .positive
    .positive:
      push rdi
      call print_uint
      pop rdi
      jmp .exit
    .negative:
      push rdi
      mov rdi, MINUS_ASCII
      call print_char
      pop rdi
      neg rdi; отрицательное число -> положительное
      jmp .positive
    .exit:
      ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax

    .next_digit:
      mov r10, [rdi]
      and r10, LAST_BYTE_MASK
      mov r11, [rsi]
      and r11, LAST_BYTE_MASK
      cmp r10, r11
      jnz .not_equals
      test r10, r10
      jz .equals
      inc rdi
      inc rsi
      jmp .next_digit

    .not_equals:
      xor rax, rax
      jmp .exit
    .equals:
      mov rax, 1
      jmp .exit
    .exit:
      ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ;
    mov rdi, STDIN_CODE
    dec rsp
    mov rsi, rsp; байты пишутся в стек
    mov rdx, 1; читаем 1 символ
    syscall
    test rax, rax; случай, когда символ не был введен
    jz .exit
    mov al, [rsp]; прочитанный символ в последние 8 битов rax
    .exit:
      inc rsp
      ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  mov r10, rsi; buffer's size - counter
  test r10, r10 
  jz .null_buffer
  ;buffer's address in rdi
  xor r11, r11; счетчик прочитанных символов
  dec r10; резервируем 1 байт под null-terminator

  .skip_tabulation:
    push rdi
    push r11
    call read_char
    pop r11
    pop rdi
    cmp rax, SPACE_ASCII
    jz .skip_tabulation
    cmp rax, TAB_ASCII
    jz .skip_tabulation
    cmp rax, NEXT_LINE
    jz .skip_tabulation
    test r10, r10; нeобходимо проверить, есть ли место для очередного байта
    jz .so_big_bro
    test rax, rax; мб пустое слово
    jz .success
    mov [rdi+r11], al; символ в последних 8 битах rax, пишем их в буфер
    dec r10; уменьшаем оставшийся размер буфера на 1
    inc r11; считаем прочитанные байты, необходимо для записи в буфер

  .next_char:
    push rdi
    push r11
    call read_char
    pop r11
    pop rdi
    cmp rax, SPACE_ASCII
    jz .success
    cmp rax, TAB_ASCII
    jz .success
    cmp rax, NEXT_LINE
    jz .success
    test rax, rax
    jz .success
    test r10, r10
    jz .so_big_bro
    mov [rdi+r11], al
    dec r10
    inc r11
    jmp .next_char

  .success:
    mov byte [rdi + r11], NULL_TERM
    mov rax, rdi
    mov rdx, r11
    jmp .exit

  .so_big_bro:
    mov byte[rdi + r11], NULL_TERM
    xor rax, rax
    jmp .exit

  .null_buffer:
    xor rax, rax
    jmp .exit

  .exit:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi; счетчик цифр (rdx напрямую юзать нельзя, т.к. mul его чейнджит)
    xor rdx, rdx; для корректного умножения
    ;rdi указатель на строку
    mov r10, DEC; для сдвига rax
    .next_digit:
      xor r11, r11
      mov r11b, [rdi + rsi]
      ;коды цифр в ASCII [48, 57]
      cmp r11b, NULL_ASCII
      jl .exit; переход, если меньше 48
      cmp r11b, NINE_ASCII
      jg .exit; переход, если больше 57
      mul r10; умножение rax на 10 (сдвиг на 1 десятичный разряд)
      sub r11, TO_ASCII
      add rax, r11
      inc rsi
      jmp .next_digit
    .exit:
      mov rdx, rsi
      ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov r11b, [rdi]
    cmp r11b, MINUS_ASCII; мб минус
    jz .read_negative_number

    .read_positive_number:
      call parse_uint
      ret

    .read_negative_number:
      ;проверяем, что после минуса нет импостерских симолов
      inc rdi
      mov r11b, [rdi]
      cmp r11b, NULL_ASCII
      jl .exit; переход, если меньше 48
      cmp r11b, NINE_ASCII
      jg .exit; переход, если больше 57
      call parse_uint
      inc rdx; учитываем знак "-"
      imul rax, -1; добавляем минус к числу
    .exit:
      ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    ;rdi - указатель на строку
    ;rsi - указатель на буфер
    ;rdx - длина буфера
    push rdi
    push rsi
    call string_length; в rax длина строки
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .fail; если размер буфера < длины строки
    .next_char:
      mov al, [rdi]; char
      mov byte [rsi], al; пишем в буфер
      inc rdi
      inc rsi
      test rax, rax; null-terminator is here
      jz .exit
      jmp .next_char
    .fail:
      xor rax, rax
      ret
    .exit:
      mov rax, rdx
      ret
; ＼(≧▽≦)／
