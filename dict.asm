%include "lib.inc"; доступ к ф-ии string_equals
%define dq_size 8

section .text

global find_word

find_word:
  .next_word:
    ;rdi - указатель на нуль-терминированную строчку
    ;rsi - указатель на начало словаря (связного списка)
    cmp rsi, 0; указатель на словарь 0 => словарь пустой
    jz .not_found
    push rdi
    push rsi
    add rsi, dq_size; мы пропускаем 8 байт (dq) с указателем на следующий элемент
                    ; и получаем значение ключа
    call string_equals; возвращает в rax 1, если равны, 0 - иначе
    pop rsi
    pop rdi
    cmp rax, 1
    jz .found
    mov rsi, [rsi]; каждый элемент словаря содержит указатель на следующий элемент
    cmp rsi, 0
    jz .not_found; достигнут конец словаря
    jmp .next_word

  .found:
    mov rax, rsi; адрес начала вхождения в словарь (не значения)
    ret

  .not_found:
    xor rax, rax
    ret
